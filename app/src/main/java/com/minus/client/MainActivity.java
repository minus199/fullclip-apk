package com.minus.client;

import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.minus.client.client.ClientImpl;
import com.minus.client.client.ClipboardService;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        //setContentView(R.layout.activity_main);
        startServiceAsClient (getCurrentFocus ());
        finish ();
    }

    private void startServiceAsClient (View view) {
        Intent intent = new Intent (this, ClipboardService.class);
        startService (intent);

        Thread clientThread = new Thread (new ClientImpl ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)));
        clientThread.start ();
    }
}
