package com.minus.client.client;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by asafb on 5/4/16.
 */
class ClipboardQueue extends ArrayBlockingQueue<String>{
    private ClipboardQueue() {
        super(1, true);
    }

    private static final class InstanceContainer{
        private static final ClipboardQueue instnace = new ClipboardQueue();
    }

    public static final ClipboardQueue getInstance(){
        return InstanceContainer.instnace;
    }
}
