package com.minus.client.client;

/**
 * Created by minus on 5/2/16.
 *
 * @author asafb

 */


import android.content.ClipboardManager;

import com.minus.client.interfaces.IClient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientImpl implements IClient {
    private Socket socket;
    private ObjectInputStream inFromServer;
    private ObjectOutputStream outToServer;
    private ClipboardManager clipboardManager;

    public ClientImpl(ClipboardManager cb) {

        this.clipboardManager = cb;
    }

    private void initSocket() throws IOException {
        //final Map<String, String> login = clientView.login();
        //socket = new Socket(login.get("host").isEmpty() ? "127.0.0.1" : login.get("host"), Integer.parseInt(login.get("port")));

        String host = "10.0.0.1";
//        String host = "192.168.1.130";
        int port = 12345;

        socket = new Socket(host, port);
        outToServer = new ObjectOutputStream(socket.getOutputStream());
        inFromServer = new ObjectInputStream(socket.getInputStream());
    }

    final public void run() {
        try {
            initSocket();
        } catch (IOException e) {
            throw new RuntimeException("Unable to start com.minus.client.client", e);
        }

       new ServerClipboardListener(clipboardManager, inFromServer).start();

        while (true) {
            try {
                String take = ClipboardQueue.getInstance().take(); // take recent data from clipboard. Passed to queue by clipboardservice
                outToServer.writeObject(take); // send to server
            } catch (InterruptedException | IOException ignored) {
                System.out.println();
            }
        }
    }

    @Override
    public void close() {
        try {
            outToServer.close();
        } catch (IOException ignored) {
        }

        try {
            inFromServer.close();
        } catch (IOException ignored) {

        }

        try {
            socket.close();
        } catch (IOException ignored) {

        }
    }
}
