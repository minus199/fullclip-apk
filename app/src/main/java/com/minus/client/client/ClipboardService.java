package com.minus.client.client;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;



public class ClipboardService extends Service {
    private ClipboardManager cb ;

    private OnPrimaryClipChangedListener listener = new OnPrimaryClipChangedListener() {
        public void onPrimaryClipChanged() {
            performClipboardCheck();
        }
    };
    private String copiedText = null;

    @Override
    public void onCreate() {
        ((ClipboardManager) getSystemService(CLIPBOARD_SERVICE)).addPrimaryClipChangedListener(listener);

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void performClipboardCheck() {
        ClipboardManager cb = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (cb.hasPrimaryClip()) {
            ClipData cd = cb.getPrimaryClip();
            CharSequence label = cd.getDescription().getLabel();
            if (label != null && label.equals("REMOTE_CHANGE")){
                return ;
            }

            boolean mimeType = cd.getDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);

            if (mimeType) {
                int itemCount = Math.max(1, cd.getItemCount());
                for (int i = 0; i < itemCount; i++) {
                    ClipData.Item itemAt = cd.getItemAt(i);
                    if (itemAt != null) {
                        copiedText = itemAt.getText().toString();

                        try {
                            ClipboardQueue.getInstance().put(copiedText);
                            Toast.makeText(this, "\"" + copiedText + "\" was copied to remote clipboard.", Toast.LENGTH_LONG).show();
                            return;
                        } catch (InterruptedException ignored) {
                            Toast.makeText(this, "unable to copy.", Toast.LENGTH_LONG).show();
                        }
                    }

                    Toast.makeText(this, "Got nothing", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


}