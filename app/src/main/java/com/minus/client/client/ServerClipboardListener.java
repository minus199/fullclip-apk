package com.minus.client.client;

import android.content.ClipData;
import android.content.ClipboardManager;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by minus on 5/7/16.
 */
public class ServerClipboardListener extends Thread {
    private final ClipboardManager clipboardManager;
    private final ObjectInputStream inFromServer;

    public ServerClipboardListener(ClipboardManager clipboardManager, ObjectInputStream inFromServer) {
        this.clipboardManager = clipboardManager;
        this.inFromServer = inFromServer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Object input = waitForClipboardData();
                setClipboardContent(input);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void setClipboardContent(Object input) {
        ClipData clip = ClipData.newPlainText("REMOTE_CHANGE", String.valueOf(input));
        clipboardManager.setPrimaryClip(clip);
    }

    private Object waitForClipboardData() throws IOException, ClassNotFoundException {
        Object input = inFromServer.readObject();

        while (input == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return input;
    }
}
