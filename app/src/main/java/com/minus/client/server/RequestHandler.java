package com.minus.client.server;


import android.content.ClipData;
import android.content.ClipboardManager;

import java.io.*;
import java.net.Socket;
import java.util.HashSet;

/**
 * @author MiNuS420 <minus199@gmail.com>
 */
class RequestHandler implements Runnable {

    private String name;
    private ClipboardManager clipboardManager;
    private final Socket socket;

    private ObjectInputStream inFromClient;
    private ObjectOutputStream outToClient;

    private static final HashSet<ObjectOutputStream> writers = new HashSet<>();

    RequestHandler(ClipboardManager systemService, Socket socket) throws IOException {
        this.clipboardManager = systemService;

        this.socket = socket;
        inFromClient = new ObjectInputStream(socket.getInputStream());
        outToClient = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        try {
            writers.add(outToClient);
            while (true) {
                Object input = inFromClient.readObject();
                while (input == null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                setClipboardContent(input);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            close();
        }
    }

    private void setClipboardContent(Object input) {
        ClipData clip = ClipData.newPlainText("clipboard text",String.valueOf(input));
       clipboardManager.setPrimaryClip(clip);
    }

    private void close() {
        if (name != null) {
            //names.remove(name);
        }
        if (outToClient != null) {
            writers.remove(outToClient);
        }
        try {
            socket.close();
        } catch (IOException e) {
        }
    }

}
