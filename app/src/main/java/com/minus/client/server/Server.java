package com.minus.client.server;

import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Created by asafb on 5/4/16.
 */
public class Server extends Service {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    final static private Logger logger = Logger.getLogger(Server.class.getName());
    final private HashSet<String> names = new HashSet<>();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                waitAndProcessRequest();
            }
        }).start();

        return START_STICKY;
    }

    void waitAndProcessRequest() {
        try (ServerSocket listener = new ServerSocket(54321)) {
            while (true) {
                executorService.submit(new RequestHandler((ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE), listener.accept()));
            }
        } catch (IOException ex) {
            //Toast.makeText(this, "Port appears to be closed(" + ex.getMessage() + "), trying to find another.", Toast.LENGTH_LONG).show();

            //waitAndProcessRequest();
        }
    }
}
