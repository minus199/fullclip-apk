/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.minus.client.interfaces;

import java.io.PrintWriter;
import java.util.Map;

/**
 * @author asafb
 */
public interface IClientView {

    IClientView toggleEnableChat();

    IClientView appendToChat(String message);


    void attachOnEnterListener(PrintWriter out);


    /**
     * Ask user for login data and return as map
     *
     * @return
     */
    Map<String, String> login();

}
